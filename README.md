# Dotnet SDK Docker Images

This project contains definitions for devcontainer docker images containing the dotnet SDK.
They provide the development environment for dotnet applications.

## Available Tags

- `8.0`, `latest`
- `6.0`
- `5.0`

## Available Platforms

All images are available for the following platforms:

* linux/amd64
