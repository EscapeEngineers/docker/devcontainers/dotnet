#!/bin/bash

tabs 6

# Ensures that changes in the credentials file are not tracked by git anymore.
git update-index --assume-unchanged .devcontainer/.env.local

# Load variables from existing file
source .devcontainer/.env.local

echo " -----------------------------------------------------------------------------"
echo "  _____                          _____             _                          "
echo " | ____|___  ___ __ _ _ __   ___| ____|_ __   __ _(_)_ __   ___  ___ _ __ ___ "
echo " |  _| / __|/ __/ _\` | '_ \ / _ \  _| | '_ \ / _\` | | '_ \ / _ \/ _ \ '__/ __|"
echo " | |___\__ \ (_| (_| | |_) |  __/ |___| | | | (_| | | | | |  __/  __/ |  \__ \\"
echo " |_____|___/\___\__,_| .__/ \___|_____|_| |_|\__, |_|_| |_|\___|\___|_|  |___/"
echo "                     |_|                     |___/                            "
echo " -----------------------------------------------------------------------------"
echo

# Check if credentials are set properly
if [ -z "${ESCAPEENGINEERS_LIBRARY_USERNAME}" ] &&
   [ -z "${ESCAPEENGINEERS_LIBRARY_PASSWORD}" ]; then
    echo -e " ⚠\tThe credentials for restoring the private packages are missing."
    echo -e "   \tPlease create a personal access token for your GitLab account before you"
    echo -e "   \tcontinue."
    echo
    echo -ne " \tYour GitLab username: "
    read ESCAPEENGINEERS_LIBRARY_USERNAME
    echo -ne " \tYour personal access token: "
    read -s ESCAPEENGINEERS_LIBRARY_PASSWORD
    echo

    updated="y"

    cat >.devcontainer/.env.local <<EOL
ESCAPEENGINEERS_LIBRARY_USERNAME=${ESCAPEENGINEERS_LIBRARY_USERNAME}
ESCAPEENGINEERS_LIBRARY_PASSWORD=${ESCAPEENGINEERS_LIBRARY_PASSWORD}
NPM_TOKEN=${ESCAPEENGINEERS_LIBRARY_PASSWORD}
EOL

    echo
    echo -e " ✓\tCredentials successfully written to the file .env.local."
    echo
else
    echo -e " ✓\tEverything is already set up. Nice!"
    echo
fi

echo " -----------------------------------------------------------------------------"
echo

dotnet restore --force --no-cache

if [ "${updated}" == "y" ]; then
    echo
    echo " -----------------------------------------------------------------------------"
    echo -e "  ⚠\tCredentials were updated! A restart of the container is enforced to"
    echo -e "    \tensure that the new credentials are being used."
    echo
    echo -e "  \t\e[1mMake sure to click \"Reload Window\".\e[0m"
    echo
    echo -e " ☕\tIn the meantime, please take a sip of your coffee while we are"
    echo -e "    \tinitializing everything."
    echo
    echo

    pkill -o bash
fi
